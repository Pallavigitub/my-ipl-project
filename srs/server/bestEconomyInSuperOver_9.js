// 9. Find the bowler with the best economy in super overs

function bestEconomy(matches,deliveries){
    const arr=[];
    for(let economy of deliveries){
        if(economy.is_super_over>0){
            arr.push(economy)
        }
    }
    //console.log(arr)

    const bowlerr={}
    arr.map(ele=>{
        if(bowlerr[ele.bowler]){
             const balls=Number(ele.ball)<=6 ? bowlerr[ele.bowler].balls+1 : bowlerr[ele.bowler].balls
             const runs=bowlerr[ele.bowler].runs+Number(ele.total_runs)
             bowlerr[ele.bowler]={balls,runs}
             //console.log(bowlerr)
        }
        else{
            const balls=1;
            const runs=Number(ele.total_runs)
            bowlerr[ele.bowler]={balls,runs}
            //console.log(bowlerr)
        }
    })

    const key=Object.keys(bowlerr)
    const superOver={}
    key.map(element=>{
        const best=((bowlerr[element].runs)/((bowlerr[element].balls)/6)).toFixed(2)
        superOver[element]=best
    })
    //console.log(superOver)
    const sorted=Object.entries(superOver).sort((a,b)=>{
        return (a[1]-b[1])
    })
    //console.log(sorted)

    let output= (Object.fromEntries(sorted))
    return output;
}
module.exports=bestEconomy