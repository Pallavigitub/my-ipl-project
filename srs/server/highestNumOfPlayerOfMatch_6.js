//6. Find a player who has won the highest number of Player of the Match awards for each season.

// function playerOfMatch(matches){
//     const obj={}
//     for (let element of matches){
//         if(obj[element.season]){
//             if(obj[element.season][element.player_of_match]){
//                 obj[element.season][element.player_of_match]++;
//             }
//             else{
//                 obj[element.season][element.player_of_match]=1;
//             }
//         } else{
//             obj[element.season]={}
//             obj[element.season][element.player_of_match]=1
//         }
//     }
//     console.log(obj)

//    const key=Object.keys(obj);
//    //console.log(key)
//    const values=Object.values(obj); 
//    //console.log(values)

//    const array=[];
//    values.forEach(ele=>{
//     array.push(Object.entries(ele))
//    });
//    //console.log(array)

//    let sorted=array.map(player=>{
//     return player.sort((a,b)=>{
//         return b[1]-a[1];
//     })[0]
//    });
//    //console.log(sorted)

//    const highestNoOfPlayer={}

//    for(let i=0;i<sorted.length;i++){
//     highestNoOfPlayer[key[i]]=sorted[i]
//    }

   
//    fs.writeFile('srs/public/output/highestNumOfPlayerOfMatch_6.json',JSON.stringify(highestNoOfPlayer,null,4),(err=>{
//     if(err){
//         console.log(err)
//     }
//    })); 
//  }
//  playerOfMatch(matches)

//------------------------------------HOF---------------------------------//

function playerOfMatch(matches){
       const obj=matches.reduce((acc,cv)=>{

        if(acc[cv.season]){
            acc[cv.season][cv.player_of_match] ? acc[cv.season][cv.player_of_match]++ : acc[cv.season][cv.player_of_match]=1
        }
        else{
            acc[cv.season]={}
            acc[cv.season][cv.player_of_match]=1
        }
        
        return acc
       },{})
       return obj;
    }   

module.exports=playerOfMatch