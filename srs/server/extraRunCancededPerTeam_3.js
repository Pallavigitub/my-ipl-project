// 3. Extra runs conceded per team in the year 2016

const deliveries=require('../public/output/deliveries.json')
const matches=require('../public/output/matches.json')
const fs=require('fs')

// function extraRunCanceded(matches,deliveries){
//     const years=[]
//     for(let year of matches){
//         if(year.season ==='2016'){
//             years.push(year.id)
//         }
//     }
//     //console.log(years)
//   const conceded ={}
//     for(let ele of deliveries){
//         if(years.includes(ele.match_id)){
//             if(!conceded[ele.bowling_team]){
//                 conceded[ele.bowling_team]=ele.extra_runs*1;
//             }
//             else{
//                 conceded[ele.bowling_team]+=ele.extra_runs*1
//             }
//         }
//     }
//     fs.writeFile('srs/public/output/extraRunCancededPerTeam_3json',JSON.stringify(conceded,null,4),(err=>{
//         if(err){
//             console.log(err)
//         }
//     }))
// }

// extraRunCanceded(matches,deliveries)

//-------------------------HOF------------------------------------//

function extraRunCanceded(matches,deliveries){
        const year=matches.filter(match => 
        match.season ==='2016').map(ele=> ele.id)
        
        return deliveries.reduce((acc,cv)=>{
            if(year.includes(cv.match_id)){
                if(!acc[cv.bowling_team]){
                    acc[cv.bowling_team]=cv.extra_runs*1
                }
                else{
                    acc[cv.bowling_team]+=cv.extra_runs*1
                }
            }
            return acc
        },{})
        
    }
        module.exports=extraRunCanceded

    
    
