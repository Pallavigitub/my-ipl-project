// 7. Find the strike rate of a batsman for each season

function strikeRateOfBatsman(matches,deliveries){
    const selectBatsman='DA Warner';
    arr=[];
    for(let batsmans of deliveries){
        if(batsmans.batsman==selectBatsman){
            arr.push(batsmans)
        }
    }
    //console.log(arr)

    const idYears={}

    matches.map(element=>{
        idYears[element.id]=element.season
    })
        //console.log(years)

        const strikeRate={}
            for(let item of arr){
                if(strikeRate[idYears[item.match_id]]){
                    const ball= strikeRate[idYears[item.match_id]].ball+1
                    const run=strikeRate[idYears[item.match_id]].run+Number(item.batsman_runs)
                }
            }
            arr.map(ele=>{
                if(strikeRate[idYears[ele.match_id]]){
                    const ball=strikeRate[idYears[ele.match_id]].ball+1;
                    const run=strikeRate[idYears[ele.match_id]].run+Number(ele.batsman_runs)
                    strikeRate[idYears[ele.match_id]]={ball,run}
                }
                else{
                    const ball=1;
                    const run=Number(ele.batsman_runs)
                    strikeRate[idYears[ele.match_id]]={ball,run}
                }
            })
            //console.log(strikeRate)
            const key=Object.keys(strikeRate)
            const strike={}
            const result={}
            key.map(res=>{
                const match=((strikeRate[res].run/strikeRate[res].ball)*100).toFixed(2)
                result[res]=match
            })
              return  strike[selectBatsman]=result;
                
            
        }          

module.exports=strikeRateOfBatsman