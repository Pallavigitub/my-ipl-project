// 1. Number of matches played per year for all the years in IPL.

// function noOfMatches(matches){
//     const totalMatches={}
//     for(let match of matches){
//         if(totalMatches[match.season]){
//             totalMatches[match.season]++;
//         } 
//         else{
//                 totalMatches[match.season]=1;
//         }
//     }

// }
// noOfMatches(matches)

//------------------------HOF------------------------------------//


function noOfMatches(matches){
        return matches.reduce((acc,cv) => {
            if(acc[cv.season]){
                acc[cv.season]++;
            }
            else{
                acc[cv.season]=1;
            }
            return acc
        },{})
    }
    module.exports=noOfMatches;

    