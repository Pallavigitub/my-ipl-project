//5. Find the number of times each team won the toss and also won the match

const matches=require('../public/output/matches.json')
const fs=require('fs')

// function tossAndMatch(matches){
//     const won={}
//     for(let element of matches){
//         if(element.toss_winner === element.winner){
//             won[element.toss_winner]++;
//         }
//         else{
//             won[element.toss_winner]=1;
//         }
//     }
//     fs.writeFile('srs/public/output/eachTeamWonTheTossAndMatch_5.json',JSON.stringify(won,null,4),(err=>{
//         if(err){
//             console.log(err)
//         }
//     }))
// }
// tossAndMatch(matches)

//----------------------------HOF----------------------------------------//

function tossAndMatch(matches){
      return matches.reduce((acc,cv)=>{
        if(cv.toss_winner === cv.winner){
            acc[cv.toss_winner]++;
        }
        else{
               acc[cv.toss_winner]=1; 
        }
        return acc;
      },{})
      
}
module.exports=tossAndMatch
