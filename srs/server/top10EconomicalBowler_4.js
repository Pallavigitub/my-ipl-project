4. //Top 10 economical bowlers in the year 2015.

const deliveries=require('../public/output/deliveries.json')
const matches=require('../public/output/matches.json')
const fs=require('fs')

function economicalBowler(matches,deliveries){
    const year=[];
    for (let matchYear of matches){
        if((matchYear.season)==='2015'){
            year.push(matchYear.id)
        }
    }
    //console.log(year)

    const matchId=[];
    year.map(ele=>{
        deliveries.filter((idEle)=>{
            if(idEle.match_id === ele){
                matchId.push(idEle)
            }
        })
    });
    //console.log(matchId)

    const bowlerr={}
    matchId.map(element=>{
        if(bowlerr[element.bowler]){
            const boll=element.ball<=6 ? bowlerr[element.bowler].boll+1 : bowlerr[element.bowler].boll;
            const runs=bowlerr[element.bowler].runs + Number(element.total_runs)
            bowlerr[element.bowler]={boll,runs}
        } 
        else{
            const boll=1;
            const runs=Number(element.total_runs);
            bowlerr[element.bowler]={boll,runs}
        }
    })
    //console.log(bowlerr)

    const key=Object.keys(bowlerr)
    const economical={}
    key.map(el=>{
        //console.log(bowlerr[el].runs)
        const economicalvalue=((bowlerr[el].runs)/((bowlerr[el].boll)/6)).toFixed(2)
        economical[el]=economicalvalue;
})
 //console.log(economical)

 let sorted=Object.entries(economical).sort((a,b)=>{
    return (a[1]-b[1])
 })
 //console.log(sorted)
 let result=(Object.fromEntries(sorted.splice(0,10)))
 
    return result;
}
module.exports=economicalBowler;