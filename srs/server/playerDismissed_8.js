// 8. Find the highest number of times one player has been dismissed by another player

function playerDismissed(deliveries){
    const arr=[];
    for(let element of deliveries){
        if(!element.player_dismissed==""){
            arr.push(element)
        }
    }
    //console.log(arr)
    const dismiss=[];
    arr.map(ele=>{
        dismiss.push(ele.player_dismissed.concat(' by ', ele.bowler))
    })
    //console.log(dismiss)

    const  res=dismiss.reduce((acc,cv)=>{
        if(acc[cv]){
            acc[cv]+=1;
        }
        else{
            acc[cv]=1;
        }
        return acc;
    },{})
    //console.log(res)

    const result={};
    let ind=0;
    let temp=0;
    for(let keys in res){
        if(res[keys]>temp){
            temp=res[keys]
            ind=keys
            
        }
    }
     result[ind]=temp;
     return result;

}
module.exports=playerDismissed
