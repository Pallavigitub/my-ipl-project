//2. Number of matches won per team per year in IPL.

const matches=require('../public/output/matches.json')
const fs = require('fs')

// function matchesWon(matches){
//     const won={}
//     for(let win of matches){
//         if(!won[win.winner]){
//             won[win.winner]={}
//         }
//         if(won[win.winner][win.season]){
//             won[win.winner][win.season]++;
//         }

//         else{
//             won[win.winner][win.season]=1;
//         }
//     }
//     fs.writeFile('srs/public/output/matchesWonPerTeamPerYear_2.json',JSON.stringify(won,null,4),(err=>{
//         if(err){
//             console.log(err)
//         }
//     }))
// }

//matchesWon(matches)

//----------------------HOF-----------------------------------//


function matchesWon(matches){
        return matches.reduce((acc,cv)=>{
            if(!acc[cv.season]){
                acc[cv.season]={}
            }
            if(acc[cv.season][cv.winner]){
                acc[cv.season][cv.winner]++
            } 
            else{
                acc[cv.season][cv.winner]=1;
            }
            return acc;
        },{})
        
    }
    module.exports=matchesWon
    